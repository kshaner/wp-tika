import buble from '@rollup/plugin-buble';
import { terser } from 'rollup-plugin-terser';

export default [
	{
		input: 'assets/js/wp-tika-extractor.js',
		external: [ 'React', 'ReactDOM' ],
		output: [
			{
				file: 'assets/js/wp-tika-extractor.min.js',
				format: 'iife',
				sourcemap: 'inline',
				plugins: [ terser() ],
				name: 'NLC.FieldSet',
				globals: {
					React: 'React',
					ReactDOM: 'ReactDOM',
				},
			},
		],
		plugins: [
			buble( {
				modules: false,
				objectAssign: true,
			} ),
		],
	},
];