<?php
/**
 * Plugin Name: WP Tika
 * Description: Integrates Apache Tika with the WordPress Media Library and other WordPress plugins.
 * Version: 0.0.1
 * Author:	Kurtis Shaner
 * Author URI: http://www.wdg.co
 * License: MIT
 * Text Domain: wptika
 * Namespace: WPTika
 */

// dont do anything outside of wordpress
if ( ! defined( 'ABSPATH' ) ) exit;

// path to this plugin
define( 'WP_TIKA_PLUGIN_DIR', __DIR__ );

// full path to this file
define( 'WP_TIKA_PLUGIN_FILE', __FILE__ );

// autoloaders
require_once WP_TIKA_PLUGIN_DIR . '/vendor/autoload.php';

// tika tika
wp_tika();
