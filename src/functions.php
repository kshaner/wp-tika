<?php

if ( ! function_exists( 'wp_tika' ) ) :

	/**
	 * Get the instance of the tika plugin
	 *
	 * @return \WPTika\Plugin
	 */
	function wp_tika() {
		return \WPTika\Plugin::get_instance();
	}
endif;

if ( ! function_exists( 'wp_tika_get_option' ) ) :
	/**
	 * Get an option from the wp tika settings
	 *
	 * @var string
	 * @return mixed
	 *
	 * @uses wp_tika
	 */
	function wp_tika_get_option( $option ) {
		return wp_tika()->get_option( $option );
	}
endif;

if ( ! function_exists ( 'wp_tika_extract' ) ) :
	/**
	 * Get the extracted attachment object
	 *
	 * @param int $post_id
	 * @param boolean $force - bypass internal cache or not [ default: false ]
	 * @return \WPTika\Attachment
	 *
	 * @uses \WPTika\Attachment
	 */
	function wp_tika_extract( $post_id, $force = false ) {
		if ( $force ) {
			$attachment = \WPTika\Attachment::get( $post_id );
			$attachment->extract();
		} else {
			$store = wp_tika()->get_store();
			$attachment = $store->get( $post_id );
		}

		return $attachment;
	}
endif;

if ( ! function_exists( 'wp_tika_get_store' ) ) :
	/**
	 * Get the configured persistent storage object
	 *
	 * @uses wp_tika
	 * @return \WPTika\Store\Store
	 */
	function wp_tika_get_store() {
		return wp_tika()->get_store();
	}
endif;

if ( ! function_exists( 'wp_tika_get_queue' ) ) :
	/**
	 * Get the queue of unextracted attachments from the configured store
	 *
	 * @uses wp_tika_get_store
	 * @return array
	 */
	function wp_tika_get_queue() {
		return wp_tika_get_store()->get_queue();
	}
endif;