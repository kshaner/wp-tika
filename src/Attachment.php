<?php

namespace WPTika;

class Attachment {

	/**
	 * Autoincrement table id
	 *
	 * @var int
	 * @access public
	 */
	public $tika_id;

	/**
	 * The post id passed in the constructor
	 *
	 * @var int
	 * @access public
	 */
	public $post_id;

	/**
	 * Date/time for the extraction
	 *
	 * @var string
	 * @access protected
	 */
	public $extracted;

	/**
	 * Date/time for the extraction in gmt
	 *
	 * @var string
	 * @access protected
	 */
	public $extracted_gmt;

	/**
	 * The extracted text from the database
	 *
	 * @var string
	 * @access protected
	 */
	public $content;

	/**
	 * The source of the attachment
	 */
	public $src;

	/**
	 * Error message for extracting content
	 *
	 * @var string|null
	 */
	public $error;

	/**
	 * Factory cache data
	 *
	 * @var array
	 */
	private static $cache = [];

	/**
	 * Caching factory
	 *
	 * @param int $id
	 * @return \WPTika\Attachment
	 */
	public static function get( $id ) {
		if ( ! isset( self::$cache[ $id ] ) ) {
			self::$cache[ $id ] = new self( $id );
		}

		return self::$cache[ $id ];
	}

	/**
	 * Process an attachment by id
	 *
	 * @param int $attachment_id
	 * @return bool - whether it was processed or not
	 */
	public static function process( $attachment_id ) {
		$attachment = self::get( $attachment_id );

		if ( $attachment->enabled() ) {
			$attachment->extract();
			$attachment->save();

			return true;
		}

		return false;
	}

	/**
	 * Grab the record on construct
	 *
	 * @param int $post_id
	 * @access public
	 */
	public function __construct( $post_id ) {
		global $wpdb;

		$this->post_id = $post_id;

		if ( 'attachment' !== get_post_type( $this->post_id ) ) {
			throw new \Exception( sprintf( '%d is not an attachment', $this->post_id ) );
		}

		$this->src = wp_get_attachment_url( $this->post_id );

		$record = wp_tika_get_store()->get( $this->post_id );

		if ( ! empty( $record ) ) {
			foreach( $record as $key => $val ) {
				$this->$key = $val;
			}
		}
	}

	/**
	 * Is the attachment enabled to be extracted
	 *
	 * @return bool
	 * @access public
	 */
	public function enabled() {
		return in_array( get_post_mime_type( $this->post_id ), wp_tika_get_option( 'mime_types' ), true );
	}

	/**
	 * Update the record with new content
	 *
	 * @param string $content
	 * @return int|false
	 * @access public
	 */
	public function update( $content ) {
		$this->content = $content;

		$result = wp_tika_get_store()->update( $this->post_id, $this->content );

		do_action( 'wptika/delete', $this->post_id, $this );

		return $result;
	}

	/**
	 * Delete the record
	 *
	 * @return int|mixed
	 * @access public
	 */
	public function delete() {
		$result = wp_tika_get_store()->delete( $this->post_id );

		do_action( 'wptika/delete', $this->post_id, $this );

		return $result;
	}

	/**
	 * Extract and return the content of the attachment
	 *
	 * @return string
	 * @access public
	 */
	public function extract() {
		try {
			$attached_file = get_attached_file( $this->post_id );

			if ( ! file_exists( $attached_file ) ) {
				throw new \Exception( sprintf( 'The file %s is missing', wp_get_attachment_url( $this->post_id ) ) );
			}

			// @see https://tika.apache.org/1.20/gettingstarted.html
			$cmd = sprintf(
				'%s -jar -Djava.awt.headless=true %s --text %s',
				wp_tika()->get_java(),
				wp_tika()->get_tika(),
				escapeshellarg( get_attached_file( $this->post_id ) )
			);

			if ( defined( 'WP_TIKA_CONFIG_PATH' ) && is_readable( constant( 'WP_TIKA_CONFIG_PATH' ) ) ) {
				$cmd .= ' --config=' . escapeshellarg( constant( 'WP_TIKA_CONFIG_PATH' ) );
			}

			// ignore stderr
			$cmd .= ' 2> /dev/null';

			$this->content = shell_exec( $cmd );

			if ( empty( $this->content ) ) {
				throw new \Exception( sprintf( 'The file "%s" was either empty or password protected', get_the_title( $this->post_id ) ) );
			}

		} catch ( \Exception $e ) {
			$this->error = $e->getMessage();
			set_transient( 'wptika_error_' . get_current_user_id(), $e->getMessage(), 10 * MINUTE_IN_SECONDS );
			return false;

		}

		if ( ! empty( $this->content ) ) {
			$this->content = trim( strip_tags( $this->content ) );
			$this->content = preg_replace( "/(?:\n\n+)/", "\n\n", $this->content );
		}

		do_action( 'wptika/extract', $this->post_id, $this );

		return $this->content;
	}

	/**
	 * Save the attachment content to the tika table
	 *
	 * @uses $wpdb
	 * @return int|false
	 * @access public
	 */
	public function save() {
		if ( ! isset( $this->content ) ) {
			$this->extract();
		}

		return wp_tika_get_store()->update( $this->post_id, $this->content );
	}
}
