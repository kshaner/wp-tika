<?php

namespace WPTika\Store;

class Postmeta implements Store {

	public static $label = 'Post Meta';

	public $content_meta_key = '_wp_tika_content';
	public $extracted_meta_key = '_wp_tika_extracted';
	public $extracted_gmt_meta_key = '_wp_tika_extracted_gmt';

	public function get( $post_id ) {
		return (object) [
			'tika_id'       => $post_id,
			'post_id'       => $post_id,
			'content'       => get_post_meta( $post_id, $this->content_meta_key, true ),
			'extracted'     => get_post_meta( $post_id, $this->extracted_meta_key, true ),
			'extracted_gmt' => get_post_meta( $post_id, $this->extracted_gmt_meta_key, true ),
		];
	}

	public function update( $post_id, $content ) {
		$updated = update_post_meta( $post_id, $this->content_meta_key, $content );

		if ( $updated ) {
			update_post_meta( $post_id, $this->extracted_meta_key, current_time( 'Y-m-d H:i:s' ) );
			update_post_meta( $post_id, $this->extracted_gmt_meta_key, current_time( 'Y-m-d H:i:s', true ) );
		}

		return $updated;
	}

	public function delete( $post_id ) {
		$deleted = delete_post_meta( $post_id, $this->content_meta_key );

		if ( $deleted ) {
			delete_post_meta( $post_id, $this->extracted_meta_key );
			delete_post_meta( $post_id, $this->extracted_gmt_meta_key );
		}

		return $deleted;
	}

	private $queue;

	public function get_queue() {
		global $wpdb;

		if ( ! isset( $this->queue ) ) {

			$mime_types = wp_tika_get_option( 'mime_types' );
			$mime_types_placeholders = implode( ', ', array_fill( 0, count( $mime_types ), '%s' ) );

			$arguments = $mime_types;
			array_push( $arguments, $this->content_meta_key );

			$sql_statement = $wpdb->prepare(
				"SELECT ID
				FROM $wpdb->posts posts
				WHERE posts.post_type = 'attachment'
				AND posts.post_mime_type IN ( $mime_types_placeholders )
				AND NOT EXISTS (
					SELECT meta_id
					FROM $wpdb->postmeta postmeta
					WHERE postmeta.post_id = posts.ID
					AND postmeta.meta_key = %s
				)",
				$arguments
			);

			$this->queue = $wpdb->get_col( $sql_statement );
		}

		return $this->queue;
	}

}
