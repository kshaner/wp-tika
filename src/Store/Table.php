<?php

namespace WPTika\Store;

class Table implements Store {

	/**
	 * The label that displays in the settings page
	 *
	 * @var string
	 * @access public
	 */
	public static $label = 'Table';

	/**
	 * The name of the sql table
	 *
	 * @var string
	 * @access private
	 */
	private $table = 'tika';

	/**
	 * A local non-persistent object-cache
	 *
	 * @var string
	 * @access private
	 */
	private $cache = [];

	/**
	 * Construct the table name with the $wpdb->prefix
	 *
	 * @access public
	 */
	public function __construct() {
		global $wpdb;
		$this->table = $wpdb->prefix . $this->table;
	}

	public function __get( $prop ) {
		if ( isset( $this->$prop ) ) {
			return $this->$prop;
		}
	}

	public function label() {
		return self::$label;
	}

	/**
	 * Get a record from the store by post_id
	 *
	 * @param int $post_id
	 * @return \StdClass
	 */
	public function get( $post_id ) {
		global $wpdb;

		if ( isset( $this->cache[ $post_id ] ) ) {
			return $this->cache[ $post_id ];
		}

		$record = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM $this->table WHERE post_id = %d ORDER BY tika_id DESC LIMIT 1",
			$post_id
		) );

		$this->cache[ $post_id ] = ! empty( $record ) ? $record : null;

		return $this->cache[ $post_id ];
	}

	/**
	 * Update a records content by post_id
	 *
	 * @param int $post_id
	 * @param string $content
	 * @return int|false - the number of rows affected or false on failure
	 */
	public function update( $post_id, $content ) {
		global $wpdb;

		$tika_id = intval( $wpdb->get_var( $wpdb->prepare(
			"SELECT tika_id FROM $this->table WHERE post_id = %d ORDER BY tika_id DESC LIMIT 1",
			$post_id
		 ) ) );

		$current_time = current_time( 'mysql' );
		$current_gmt  = get_gmt_from_date( $current_time );

		$record = [
			'post_id'       => $post_id,
			'extracted'     => $current_time,
			'extracted_gmt' => $current_gmt,
			'content'       => $content,
		];

		$record_formats = [ '%d', '%s', '%s', '%s' ];

		if ( $tika_id > 0 ) {
			$db_result = $wpdb->update( $this->table, $record, [ 'tika_id' => $tika_id ], $record_formats, [ '%d' ] );

			if ( intval( $db_result ) > 0 ) {
				$db_result = $tika_id;
			}
		} else {
			$where_format = [ '%d', '' ];

			$db_result = $wpdb->insert(	$this->table, $record, $record_formats );
		}

		if ( isset ( self::$cache[ $post_id ] ) ) {
			unset( self::$cache[ $post_id ] );
		}

		return $db_result;
	}

	/**
	 * Delete a record by post_id
	 *
	 * @param int $post_id
	 * @return int|false - number of rows deleted or false on failure
	 */
	public function delete( $post_id ) {
		global $wpdb;

		if ( empty( $post_id ) ) {
			return false;
		}

		if ( isset ( self::$cache[ $post_id ] ) ) {
			unset( self::$cache[ $post_id ] );
		}

		return $wpdb->delete( $this->table, [ 'post_id' => $post_id ] );
	}

	/**
	 * Get a queue of attachments yet to be extracted
	 *
	 * @return array
	 */
	public function get_queue() {
		global $wpdb;

		$mimes = wp_tika_get_option( 'mime_types' );
		$mimes_in = implode( ',', array_fill( 0, count( $mimes ), '%s' ) );

		$sql = $wpdb->prepare(
			"SELECT ID FROM $wpdb->posts
			WHERE post_type = 'attachment'
			AND post_mime_type IN ($mimes_in)
			AND NOT EXISTS ( SELECT post_id FROM wp_tika WHERE $wpdb->posts.ID = $this->table.post_id )
			ORDER BY ID DESC",
			$mimes
		);

		return $wpdb->get_col( $sql );
	}

	public static function create_table() {
		global $wpdb;
		$self = new self();

		$sql = trim(
			"CREATE TABLE $self->table (
				`tika_id` bigint(20) NOT NULL AUTO_INCREMENT,
				`post_id` bigint(20) NOT NULL,
				`extracted` datetime(0) NOT NULL,
				`extracted_gmt` datetime(0) NOT NULL,
				`content` longtext NULL,
				PRIMARY KEY  (tika_id),
				KEY `post_id` (post_id)
			) COLLATE {$wpdb->collate}
		");

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		dbDelta( $sql );

		wp_cache_flush();
	}

	public static function drop_table() {
		// delete options and custom table
		global $wpdb;
		$self = new self();

		return $wpdb->query( sprintf( 'DROP TABLE IF EXISTS %s', $self->table ) );
	}

}