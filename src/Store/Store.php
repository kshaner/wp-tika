<?php

namespace WPTika\Store;

interface Store {

	/**
	 * Get a record from the store by post_id
	 *
	 * @param int $post_id
	 * @return \StdClass
	 */
	public function get( $post_id );

	/**
	 * Update a records content by post_id
	 *
	 * @param int $post_id
	 * @param string $content
	 * @return int|false - the number of rows affected or false on failure
	 */
	public function update( $post_id, $content );

	/**
	 * Delete a record by post_id
	 *
	 * @param int $post_id
	 * @return int|false - number of rows deleted or false on failure
	 */
	public function delete( $post_id );

	/**
	 * Get a queue of attachments yet to extracted
	 *
	 * @return array
	 */
	public function get_queue();

}