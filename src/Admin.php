<?php

namespace WPTika;

class Admin {

	/**
	 * Capability required for managing the WPTika settings
	 *
	 * @var string
	 * @access protected
	 * @see $this->__construct for the wptika/capability filter
	 */
	protected $capability = 'manage_options';

	/**
	 * The admin menu slug for the plugin
	 *
	 * @var string
	 * @access protected
	 */
	protected $menu_slug  = 'wptika';

	/**
	 * The title of the admin menu
	 *
	 * @var string
	 * @access protected
	 */

	protected $menu_title = 'WP Tika';

	/**
	 * The title of the settings page
	 *
	 * @var string
	 * @access protected
	 */
	protected $title = 'WP Tika';

	/**
	 * Holds a reference to the plugin class for configuration
	 *
	 * @var \WPTika\Plugin
	 * @access protected
	 */
	protected $plugin;

	/**
	 * The nonce key used in the extractor process
	 *
	 * @var string
	 * @access protected
	 */
	protected $extract_nonce_key = 'tika_extract';

	/**
	 * This class hooks into wp-admin for extracting/editing/viewing attachment content
	 */
	public function __construct( \WPTika\Plugin $plugin ) {
		$this->plugin = $plugin;

		$this->capability = apply_filters( 'wptika/capability', $this->capability );

		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'wp_ajax_tika_extract', [ $this, 'wp_ajax_tika_extract' ] );
		add_action( 'add_attachment', [ $this, 'add_attachment' ] );
		add_action( 'admin_notices', [ $this, 'admin_notices' ] );
		add_action( 'delete_attachment', [ $this, 'delete_attachment' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
		add_filter( 'attachment_fields_to_edit', [ $this, 'attachment_fields_to_edit' ], 10, 2 );
		add_filter( 'attachment_fields_to_save', [ $this, 'attachment_fields_to_save' ], 10, 2 );
	}

	/**
	 * Register settings
	 *
	 * @return void
	 * @access public
	 * @action admin_init
	 */
	public function admin_init() {
		register_setting( $this->plugin->option_key, $this->plugin->option_key );
		add_settings_section( $this->menu_slug, '', '__return_empty_string', $this->menu_slug );

		add_settings_field( 'store', 'Store', [ $this, 'render_field__storage' ], $this->menu_slug, $this->menu_slug );
		add_settings_field( 'mimes', 'Mime Types', [ $this, 'render_field__mimes' ], $this->menu_slug, $this->menu_slug );
	}

	/**
	 * Add admin menu pages
	 *
	 * @return void
	 * @access public
	 * @action admin_menu
	 */
	public function admin_menu() {
		$icon_path = WP_TIKA_PLUGIN_DIR . '/assets/svg/dashicon.svg';
		$icon = file_exists( $icon_path ) ? 'data:image/svg+xml;base64,' . base64_encode( file_get_contents( $icon_path ) ) : 'dashicons-media-text';

		add_menu_page( $this->menu_title, $this->menu_title, $this->capability, $this->menu_slug, [ $this, 'render_page' ], $icon, 80 );
	}

	/**
	 * Extract text on upload
	 *
	 * @param int $post_id
	 * @access public
	 * @return bool
	 * @action add_attachment
	 */
	public function add_attachment( $attachment_id ) {
		return Attachment::process( $attachment_id );
	}

	/**
	 * Delete the extracted contents from the store on attachment deleted
	 *
	 * @param int $attachment_id
	 * @return boolean
	 * @access public
	 * @action delete_attachment
	 */
	public function delete_attachment( $attachment_id ) {
		$attachment = Attachment::get( $attachment_id );
		return $attachment->delete();
	}

	/**
	 * Add our admin notice errors
	 *
	 * @action admin_notices
	 */
	public function admin_notices() {
		$transient_key = 'wptika_error_' . get_current_user_id();

		if ( $transient = get_transient( $transient_key ) ) {
			printf(
				'<div class="notice notice-error is-dismissible">%s</div>',
				wpautop( 'Error extracting text from upload: ' . esc_html( $transient ) )
			);

			delete_transient( $transient_key );
		}
	}

	/**
	 * Enqueue our css file
	 *
	 * @return void
	 * @action admin_enqueue_scripts
	 */
	public function admin_enqueue_scripts() {
		wp_register_script( 'wp-tika-extract', plugins_url( 'assets/js/wp-tika-extractor.min.js', WP_TIKA_PLUGIN_FILE ), [ 'react', 'react-dom' ], filemtime( WP_TIKA_PLUGIN_DIR . '/assets/js/wp-tika-extractor.min.js' ), true );
		wp_enqueue_style( 'wp-tika', plugins_url( 'assets/css/wp-tika.css', WP_TIKA_PLUGIN_FILE ), [], filemtime( WP_TIKA_PLUGIN_DIR . '/assets/css/wp-tika.css' ) );
	}

		/**
	 * Callback for rendering the settings page
	 *
	 * @return void
	 * @access public
	 */
	public function render_page() {
		if ( ! current_user_can( $this->capability ) ) {
			wp_die( 'Nope' );
		}

		$tabs = [
			'settings' => [
				'active' => false,
				'class'  => 'nav-tab',
				'title'  => 'Settings',
				'href'   => add_query_arg( 'page', $this->menu_slug, admin_url( 'admin.php' ) ),
				'icon'   => 'admin-generic',
				'render' => [ $this, 'render_settings' ],
			],
			'extract' => [
				'active' => false,
				'class'  => 'nav-tab',
				'title'  => 'Extract',
				'href'   => add_query_arg( [ 'page' => $this->menu_slug, 'tab'  => 'extract' ], admin_url( 'admin.php' ) ),
				'icon'   => 'hammer',
				'render' => [ $this, 'render_extract' ],
			],
		];

		$tab = isset( $_GET['tab'] ) && isset( $tabs[ $_GET['tab'] ] ) ? wp_unslash( $_GET['tab'] ) : 'settings';
		$tabs[ $tab ]['active'] = true;
		$tabs[ $tab ]['class'] .= ' nav-tab-active';
		?>
		<div class="wrap">
			<h1><?php esc_html_e( get_admin_page_title(), 'wptika' ); ?></h1>

			<?php settings_errors( $this->menu_slug ); ?>

			<h2 class="nav-tab-wrapper">
				<?php foreach ( $tabs as &$tabdata ) : ?>
					<a class="<?= esc_attr( $tabdata['class'] ) ?>" href="<?= esc_url( $tabdata['href'] ) ?>">
						<span class="dashicons dashicons-<?= esc_attr( $tabdata['icon'] ) ?>"></span>
						<?php esc_html_e( $tabdata['title'], 'wptika' ); ?>
					</a>
				<?php endforeach; ?>
			</h2>

			<?php
			switch( $tab ) :
				case 'extract':
					wp_enqueue_script( 'wp-tika-extract' );

					$extractor = new Extractor( 10, 0 );
					$extractor->queue_all();
					$queue = wp_tika_get_queue();
					$queue_count = count( $queue );
					?>
					<div class="notice notice-info">
						<p>Pro Tip: Use WP CLI to extract faster at the command line with <code>wp tika extract</code></p>
					</div>

					<div id="tika-extractor" data-url="<?= esc_url( add_query_arg( 'action', 'tika_extract', admin_url( 'admin-ajax.php' ) ) ) ?>" data-total="<?= esc_attr( $extractor->total ) ?>" data-queue="<?= esc_attr( $queue_count ) ?>" data-nonce="<?= wp_create_nonce( $this->extract_nonce_key ) ?>"></div>
				<?php
				break;
				default:
				?>
					<form action="options.php" method="post">
					<?php
						settings_fields( $this->plugin->option_key );
						do_settings_sections( $this->menu_slug );
						submit_button( 'Save' );
					?>
					</form>
				<?php
				break;
			endswitch;
			?>

			<hr>

			<p class="tika-attribution">
				<img src="<?= esc_url( plugins_url( 'assets/svg/powered-by-apache.svg', WP_TIKA_PLUGIN_FILE ) ); ?>" alt="Apache Tika" height="64" width="64" loading="lazy">
				<small>Powered by Apache Tika™.<br> Apache®, Apache Tika™, and the feather logo are either registered trademarks or trademarks of the Apache Software Foundation in the United States and/or other countries.</small>
			</p>
		</div>
		<?php
	}

	/**
	 * Callback for rendering the storage select field
	 *
	 * @return void
	 * @access public
	 */
	public function render_field__storage() {
		$settings = $this->plugin->get_option();
		?>

		<select name="<?php echo esc_attr( $this->plugin->option_key ); ?>[store]" class="widefat">
			<?php foreach( $this->plugin->stores as $store => $class ) : ?>
			<option value="<?php echo esc_attr( $store ); ?>"<?php if ( $store === $settings['store'] ) : ?> selected<?php endif; ?>><?php esc_html_e( $class::$label, 'wptika' ); ?></option>
			<?php endforeach; ?>
		</select>

		<ul>
			<li>Use <code>Table</code> (default) for the best performance.</li>
			<li>Use <code>Post Meta</code> if you can not create a custom table.</li>
		</ul>
		 <?php
	}

	/**
	 * Callback for rendering the mime types checkboxes
	 *
	 * @return void
	 * @access public
	 */
	public function render_field__mimes() {
		$settings   = $this->plugin->get_option();
		$mime_types = $this->plugin->get_mime_types();

		foreach( $mime_types as $mime_type ) {
			printf(
				'<label><input type="checkbox" name="%1$s[mime_types][]" value="%2$s"%3$s> %2$s</label><br>',
				$this->plugin->option_key,
				$mime_type,
				in_array( $mime_type, $settings['mime_types'], true ) ? ' checked' : ''
			);
		}
	}

	/**
	 * Add our extracted content as a textarea
	 *
	 * @param array $fields
	 * @param \WP_Post $post
	 * @return array
	 */
	public function attachment_fields_to_edit( $fields, $post ) {
		$attachment = Attachment::get( $post->ID );

		return array_merge( $fields, [
			'wptika' => [
				'input' => 'textarea',
				'label' => __( 'Extracted text from Tika' ),
				'value' => $attachment->content,
			]
		] );
	}

	/**
	 * Save our modified data in the attachment_fields_to_save filter
	 *
	 * @param array $post
	 * @param array $data
	 * @return array
	 * @access public
	 * @filter attachment_fields_to_save
	 */
	public function attachment_fields_to_save( $post, $data ) {
		if ( isset( $data['wptika'] ) ) {
			try {
				$attachment = Attachment::get( intval( $post['post_ID'] ) );
				if ( $attachment->enabled() ) {
					$attachment->update( sanitize_textarea_field( $data['wptika'] ) );
				}
			} catch( \Exception $e ) {
				//
			}
		}

		return $post;
	}

	/**
	 * Ajax handler for extract settings action
	 *
	 * @return void
	 * @access public
	 * @action wp_ajax_tika_extract
	 */
	public function wp_ajax_tika_extract() {
		check_ajax_referer( $this->extract_nonce_key );

		$batch         = max( 1, intval( wp_unslash( $_REQUEST['batch'] ) ) );
		$mode          = wp_unslash( $_REQUEST['mode'] );
		$transient_key = md5( implode( '|', [ __FUNCTION__, $batch, $mode ] ) );

		$extractor = get_transient( $transient_key );

		if ( empty( $extractor ) ) {
			$extractor = new Extractor( $batch, 0 );

			switch( $mode ) {
				case 'all':
					$extractor->queue_all();
				break;
				case 'missing':
					$extractor->queue_missing();
				break;
			}
		}

		if ( ! $extractor->is_empty() ) {
			$results = $extractor->batch();
		} else {
			$results = 'No attachments found';
		}

		if ( $extractor->is_empty() ) {
			delete_transient( $transient_key );
		} else {
			set_transient( $transient_key, $extractor, 300 );
		}

		wp_send_json_success( [
			'isEmpty' => $extractor->is_empty(),
			'queue'   => $extractor->queue,
			'results' => $results,
		] );

		exit;
	}
}
