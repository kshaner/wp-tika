<?php

namespace WPTika\Plugins;

class SearchWP {

	/**
	 * Hook into pre_get_posts if we're supposed to act
	 */
	public function __construct() {
		add_filter( 'searchwp_external_pdf_processing', [ $this, 'searchwp_external_pdf_processing' ], 10, 3 );
		add_filter( 'searchwp_purge_pdf_content', '__return_true' );
	}

	/**
	 * Add inherit to the list of post_stati and add or join and where filters
	 *
	 * @param WP_Query $wp_query
	 * @return void
	 * @action pre_get_posts
	 */
	public function searchwp_external_pdf_processing( $content, $filename, $post_id ) {
		$attachment = new \WPTika\Attachment( $post_id );

		if ( $attachment->enabled() ) {
			$content = $attachment->content;
		}

		return $content;
	}

}
