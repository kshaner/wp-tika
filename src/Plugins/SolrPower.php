<?php

namespace WPTika\Plugins;

class SolrPower {

	public function __construct() {
		add_filter( 'solr_build_document', [ $this, 'solr_build_document' ], 11, 2 );

		if ( ! empty( $_ENV['PANTHEON_ENVIRONMENT'] ) && ! defined( 'WP_TIKA_PATH' ) ) {
			define( 'WP_TIKA_PATH', '/srv/bin/tika-app-1.18.jar' );
		}
	}

	/**
	 * Add our extracted media content to the solr document post_content field
	 *
	 * @param Solarium\QueryType\Update\Query\Document\Document $document - solr generated document
	 * @param WP_Post $post
	 * @return Solarium\QueryType\Update\Query\Document\Document
	 *
	 * @filter solr_build_document
	 */
	public function solr_build_document( $document, $post ) {
		if ( 'attachment' !== $post->post_type ) {
			return $document;
		}

		$attachment = \WPTika\Attachment::get( $post->ID );
		if ( $attachment->enabled() && ! empty( $attachment->content ) ) {
			$document->setField( 'post_content', trim( strip_tags( $post->post_content . PHP_EOL . PHP_EOL . $attachment->content ) ) );
		}

		return $document;
	}
}
