<?php

namespace WPTika\Plugins;

class EnableMediaReplace {

	public function __construct() {
		add_action( 'wp_handle_replace', [ $this, 'wp_handle_replace' ], 11, 2 );
	}

	/**
	 * Re-extract the content when using enable-media-replace
	 *
	 * @param int $post_id
	 * @filter wp_handle_replace
	 */
	public function wp_handle_replace( $post_id ) {
		$attachment = new \WPTika\Attachment( $post_id );

		if ( $attachment->enabled() ) {
			$attachment->extract();
			$attachment->save();
		}
	}
}
