<?php

namespace WPTika\Plugins;

class WordPress {

	/**
	 * Hook into pre_get_posts if we're supposed to act
	 */
	public function __construct() {
		add_action( 'pre_get_posts', [ $this, 'pre_get_posts' ] );
	}

	/**
	 * Add inherit to the list of post_stati and add or join and where filters
	 *
	 * @param WP_Query $wp_query
	 * @return void
	 * @access public
	 * @action pre_get_posts
	 */
	public function pre_get_posts( $wp_query ) {
		if ( $wp_query->is_search() && ! empty( $wp_query->get( 's' ) ) ) {
			$stati = ! empty( $wp_query->get( 'post_status' ) ) ? (array) $wp_query->get( 'post_status' ) : [ 'publish' ];

			array_push( $stati, 'inherit' );

			$wp_query->set( 'post_status', $stati );

			add_filter( 'posts_join', [ $this, 'posts_join' ], 10, 2 );
			add_filter( 'posts_where', [ $this, 'posts_where' ], 10, 2 );
			add_filter( 'posts_search_orderby', [ $this, 'posts_search_orderby' ], 10, 2 );
		}
	}

	/**
	 * Add our SQL Join to WP_Query
	 *
	 * @param string $join
	 * @param WP_Query $wp_query
	 * @return string
	 * @access public
	 * @filter posts_join
	 */
	public function posts_join( $join, $wp_query ) {
		global $wpdb;

		// remove our filter so subsequent queries are checked again
		remove_filter( 'posts_join', [ $this, 'posts_join' ], 10, 2 );

		switch( wp_tika_get_option('store') ) {
			case 'table':
				$join = trim( $join ) . sprintf( " LEFT JOIN %s AS tika on $wpdb->posts.ID = tika.post_id", wp_tika_get_store()->table );
			break;
			case 'postmeta':
				$join = trim( $join ) . " LEFT JOIN $wpdb->postmeta AS tika on $wpdb->posts.ID = tika.post_id";
			break;
		}

		return $join;
	}

	/**
	 * Add our SQL WHERE to WP_Query
	 *
	 * @param string $where
	 * @param WP_Query $wp_query
	 * @return string
	 * @access public
	 * @filter posts_where
	 */
	public function posts_where( $where, $wp_query ) {
		global $wpdb;

		// remove our filter so subsequent queries are checked again
		remove_filter( 'posts_where', [ $this, 'posts_where' ], 10, 2 );

		// shortcut for current search
		$s = $wp_query->get('s');

		// multiple search terms - regex pattern is from WP_Query->parse_search
		if ( preg_match_all( '/".*?("|$)|((?<=[\t ",+])|^)[^\t ",+]+/', $s, $matches ) ) {
			$terms = $matches[0];
		} else {
			$terms = [ $s ];
		}

		foreach( $terms as $term ) {
			$like = '%' . $wpdb->esc_like( $term ) . '%';
			$pattern = $wpdb->prepare( "/ OR \($wpdb->posts.post_content LIKE %s\)/", $like );

			$replacement = sprintf(
				"$1 OR ( %s LIKE %s )",
				$this->get_table_column(),
				$wpdb->prepare( '%s', $like )
			);

			$where = preg_replace( $pattern, $replacement, $where );
		}

		return $where;
	}

	/**
	 * Modify the orderby clause
	 *
	 * @param string $orderby
	 * @param WP_Query $wp_query
	 * @return string
	 * @access public
	 * @filter posts_search_orderby
	 */
	public function posts_search_orderby( $orderby, $wp_query ) {
		global $wpdb;

		// remove our filter so subsequent queries are checked again
		remove_filter( 'posts_search_orderby', [ $this, 'posts_search_orderby' ], 10, 2 );

		// match the end of the query for the else and insert our tika.content order case
		$pattern = '/ELSE (\d+) END\)$/';

		if ( preg_match( $pattern, $orderby, $matches ) ) {
			$like = '%' . $wpdb->esc_like( $wp_query->get('s') ) . '%';

			$replacement = sprintf(
				"WHEN %s LIKE %s THEN %d ELSE %d END)",
				$this->get_table_column(),
				$wpdb->prepare( '%s', $like ),
				$matches[1],
				intval( $matches[1] ) + 1
			);

			$orderby = preg_replace( $pattern, $replacement, $orderby );
		}

		return $orderby;
	}

	/**
	 * Get the table column that is joined based on the configured storage
	 *
	 * @return string
	 * @access private
	 */
	private function get_table_column() {
		global $wpdb;

		$store = wp_tika_get_option( 'store' );

		if ( 'table' === $store ) {
			return 'tika.content';
		}

		if ( 'postmeta' === $store ) {
			return 'tika.meta_value';
		}

		return "$wpdb->posts.post_content";
	}

}
