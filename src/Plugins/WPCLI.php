<?php

namespace WPTika\Plugins;

/**
 * Extract contents from the media library to the database with Apache Tika
 */

 class WPCLI extends \WP_CLI_Command {

	const COMMAND = 'tika';

	/**
	 * Get information about the Tika configuration
	 *
	 * [--format=<format>]
	 * : Display the info in a particular format
	 * ---
	 * default: table
	 * options:
	 *  - table
	 *  - csv
	 *  - json
	 *  - yaml
	 * ---
	 */
	public function info( $args, $assoc_args ) {
		$wp_tika = wp_tika();
		$mime_types_count = count( wp_tika_get_option( 'mime_types' ) );

		$extractor = new \WPTika\Extractor( 10, 0 );
		$extractor->queue_all();
		$queue = wp_tika_get_queue();
		$queue_count = count( $queue );

		$data = [
			'Path to Tika'            => $wp_tika->get_tika(),
			'Path to Java'            => $wp_tika->get_java(),
			'Tika Config File'        => $wp_tika->get_tika_config(),
			'Extraction Disabled'     => boolval( $wp_tika->get_tika_extract_disable() ) ? 'true' : 'false',
			'Storage'                 => wp_tika_get_option( 'store' ),
			'Mime Types'              => sprintf( '%d mime type%s enabled - see `wp tika mime_types`', $mime_types_count, $mime_types_count === '1' ? '' : 's' ),
			'Total Attachments'       => $extractor->total,
			'Extracted Attachments'   => $extractor->total - $queue_count,
			'Unextracted Attachments' => $queue_count,
		];

		if ( 'table' === $assoc_args['format'] ) {
			$data = array_map( function( $key, $val ) {
				return [ 'Setting' => $key, 'Value' => $val ];
			}, array_keys( $data ), array_values( $data ) );
			$columns = array_keys( current( $data ) );
		} else {
			$columns = array_keys( $data );
			$data = [ $data ];
		}

		\WP_CLI\Utils\format_items( $assoc_args['format'], $data, $columns );
	}

	/**
	 * Show a list of configured mime types
	 *
	 * [--active]
	 * : Show active mime types and not all
	 * ---
	 * default: false
	 * ---
	 *
	 * [--format=<format>]
	 * : Display the info in a particular format
	 * ---
	 * default: table
	 * options:
	 *  - table
	 *  - csv
	 *  - json
	 *  - yaml
	 * ---
	 */
	public function mime_types( $args, $assoc_args ) {
		$mime_types = wp_tika()->get_mime_types();
		$enabled    = wp_tika_get_option( 'mime_types' );
		$format     = $assoc_args['format'];
		$colorized  = false;
		$columns    = [ 'mime_type' ];
		$data       = [];

		if ( ! empty( $assoc_args['active'] ) ) {
			foreach( $enabled as $mime_type ) {
				array_push( $data, [ 'mime_type' => $mime_type ] );
			}
		} else {
			array_push( $columns, 'active' );

			foreach( $mime_types as $mime_type ) {
				$row = [
					'mime_type' => $mime_type,
					'active' => in_array( $mime_type, $enabled, true ) ? '✓' : '✕',
				];

				if ( 'table' === $format ) {
					$colorized = true;
					$color = $row['active'] === '✓' ? '%g' : '%r';
					foreach( $row as &$val ) {
						$val = \WP_CLI::colorize( $color . $val . '%n' );
					}
				}

				array_push( $data, $row );
				unset( $row );
			}
		}

		$formatter = new \WP_CLI\Formatter( $assoc_args, $columns );
		$formatter->display_items( $data, $colorized );
	}

	/**
	 * Extact contents from attachments
	 *
	 * [<id>...]
	 * : One more more attachement ids to extract (defaults to all missing attachments)
	 *
	 * [--all]
	 * : (Re)Extract all attachments
	 *
	 * [--progress]
	 * : Show a progress bar instead of showing the results of each file
	 *
	 * [--show]
	 * : Show the extracted contents in stdout
	 */
	public function extract( $args, $assoc_args ) {
		$assoc_args = array_merge( [
			'batch' => 10,
			'show' => false,
		],$assoc_args );

		$extractor = new \WPTika\Extractor( 1, 0 );

		if ( ! empty( $assoc_args['all'] ) ) {
			$extractor->queue_all();
		} else if ( ! empty( $args ) ) {
			$mime_types = wp_tika_get_option( 'mime_types' );

			$args = array_filter( $args, function( $post_id ) use ( $mime_types ) {
				if ( ! is_numeric( $post_id ) ) {
					return false;
				}

				if ( 'attachment' !== get_post_type( $post_id ) ) {
					return false;
				}

				return in_array( get_post_mime_type( $post_id ), $mime_types, true );
			} );

			$extractor->set_queue( $args );
		} else {
			$extractor->queue_missing();
		}

		if ( $extractor->is_empty() ) {
			\WP_CLI::error( 'Found no attachments to extract' );
		}

		$total = $extractor->total;

		if ( isset( $assoc_args['progress'] ) ) {
			$progress = \WP_CLI\Utils\make_progress_bar( 'Extacting Media Contents', $total );
		}

		$errors = [];

		$index = 0;
		while( ! $extractor->is_empty() ) {
			$index++;
			$batch = $extractor->batch();

			foreach( $batch as $extracted ) {
				if ( isset( $assoc_args['progress'] ) ) {
					$progress->tick();
				}

				if ( ! empty( $extracted->error ) ) {
					if ( ! isset( $assoc_args['progress'] ) ) {
						\WP_CLI::warning( $extracted->error );
					}

					array_push( $errors, $extracted );
				} else if ( ! isset( $assoc_args['progress'] ) ) {
					\WP_CLI::success( sprintf( '(%d of %d) Extracted %d - %s', $index, $total, $extracted->post_id, wp_get_attachment_url( $extracted->post_id ) ) ) ;

					if ( $assoc_args['show'] ) {
						\WP_CLI::line( $extracted->content );
					}
				}
			}
		}

		if ( isset( $assoc_args['progress'] ) ) {
			$progress->finish();
		}

		if ( ! empty( $errors ) ) {
			$err_count = count($errors);
			\WP_CLI::warning( sprintf( 'Finished indexing %d files with %d %s', $total - $err_count, $err_count, $err_count === 1 ? 'error' : 'errors' ) );
			\WP_CLI::line( sprintf( 'IDs of Errors: %s', implode(', ', array_column( $errors, 'post_id' ) ) ) );
		}

		\WP_CLI::success( sprintf( 'Finished extracting %d files', $total ) );
	}
}
