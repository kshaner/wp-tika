<?php
/**
 * The main interface of WP Tika
 *
 * This class can be accessed through the wp_tika() helper function
 *
 * @package WPTika
 */

namespace WPTika;

class Plugin {

	/**
	 * The instance of the plugin
	 *
	 * @var \WPTika\Plugin
	 */
	private static $_instance;

	/**
	 * Only allow a single instance of the plugin class
	 *
	 * @return \WPTika\Plugin
	 */
	public static function get_instance() {
		if ( ! isset( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Holds the admin class
	 *
	 * @var \WPTika\Admin
	 * @access protected
	 */
	protected $admin;

	/**
	 * Holds the key for the settings in the options table
	 *
	 * @var string
	 * @access protected
	 */
	protected $option_key = 'wptika';

	/**
	 * Holds the active plugins
	 *
	 * @var array
	 * @access protected
	 */
	protected $plugins = [];

	/**
	 * Array of available persistent stores.
	 *
	 * Format is key => fully qualified class name
	 *
	 * Use the wptika/stores filter to add additional instances of the \WPTika\Store\Store interface
	 *
	 * @var array
	 * @access protected
	 */
	protected $stores = [
		'table' => '\WPTika\Store\Table',
		'postmeta' => '\WPTika\Store\Postmeta',
	];

	/**
	 * Default options for the wordpress settings api
	 *
	 * @var array
	 * @access protected
	 */
	protected $defaults = [
		'store' => 'table',
		'mime_types' => [
			'application/msword',
			'application/onenote',
			'application/oxps',
			'application/pdf',
			'application/rtf',
			'application/ttaf+xml',
			'application/vnd.apple.keynote',
			'application/vnd.apple.numbers',
			'application/vnd.apple.pages',
			'application/vnd.ms-access',
			'application/vnd.ms-excel',
			'application/vnd.ms-excel.addin.macroEnabled.12',
			'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
			'application/vnd.ms-excel.sheet.macroEnabled.12',
			'application/vnd.ms-excel.template.macroEnabled.12',
			'application/vnd.ms-powerpoint',
			'application/vnd.ms-powerpoint.addin.macroEnabled.12',
			'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
			'application/vnd.ms-powerpoint.slide.macroEnabled.12',
			'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
			'application/vnd.ms-powerpoint.template.macroEnabled.12',
			'application/vnd.ms-project',
			'application/vnd.ms-word.document.macroEnabled.12',
			'application/vnd.ms-word.template.macroEnabled.12',
			'application/vnd.ms-write',
			'application/vnd.ms-xpsdocument',
			'application/vnd.oasis.opendocument.chart',
			'application/vnd.oasis.opendocument.database',
			'application/vnd.oasis.opendocument.formula',
			'application/vnd.oasis.opendocument.graphics',
			'application/vnd.oasis.opendocument.presentation',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/vnd.oasis.opendocument.text',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'application/vnd.openxmlformats-officedocument.presentationml.slide',
			'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
			'application/vnd.openxmlformats-officedocument.presentationml.template',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
			'application/wordperfect',
			'text/calendar',
			'text/css',
			'text/csv',
			'text/html',
			'text/plain',
			'text/richtext',
			'text/tab-separated-values',
			'text/vtt',
		]
	];

	/**
	 * Instance of the configured storage object
	 *
	 * @var \WPTika\Store
	 * @access protected
	 */
	protected $_store;

	/**
	 * Initialize plugin & integrations
	 *
	 * @access protected
	 */
	protected function __construct() {
		if ( is_admin() ) {
			$this->admin = new Admin( $this );
		}

		add_action( 'init', [ $this, 'init' ], 99 );

		register_activation_hook( WP_TIKA_PLUGIN_FILE, [ __CLASS__, 'activation_hook' ] );
		register_deactivation_hook( WP_TIKA_PLUGIN_FILE, [ __CLASS__, 'deactivation_hook' ] );
		register_uninstall_hook( WP_TIKA_PLUGIN_FILE, [ __CLASS__, 'uninstall_hook' ] );
	}

	/**
	 * Dynamic getter for access to read-only properties
	 *
	 * @param string $prop
	 * @return mixed
	 * @access public
	 */
	public function __get( $prop ) {
		if ( isset( $this->$prop ) ) {
			return $this->$prop;
		}
	}

	/**
	 * Get the instance of the configured store
	 *
	 * @return \WPTika\Store\Store
	 * @access public
	 */
	public function get_store() {
		if ( ! isset( $this->_store ) ) {
			$store = $this->get_option( 'store' );

			if ( ! empty( $store ) && ! empty( $this->stores[ $store ] ) && class_exists( $this->stores[ $store ] ) ) {
				$this->_store = new $this->stores[ $store ];
			} else {
				$this->_store = $this->stores[ $this->defaults['store'] ];
			}
		}

		return $this->_store;
	}

	/**
	 * Get the path to the java executable. Default is assumed to be in the path of php
	 *
	 * @return string
	 */
	public function get_java() {
		if ( defined( 'WP_TIKA_JAVA_PATH' ) ) {
			return constant( 'WP_TIKA_JAVA_PATH' );
		}

		return 'java';
	}

	/**
	 * Get the path to the tika jar
	 *
	 * @return string
	 * @access public
	 */
	public function get_tika() {
		if ( defined( 'WP_TIKA_PATH' ) ) {
			return WP_TIKA_PATH;
		}

		if ( is_readable( WP_TIKA_PLUGIN_DIR . '/bin/tika-app-1.20.jar' ) ) {
			return WP_TIKA_PLUGIN_DIR . '/bin/tika-app-1.20.jar';
		}

		return false;
	}

	/**
	 * Get the path to the tika config file
	 *
	 * @return string
	 * @access public
	 */
	public function get_tika_config() {
		// set this constant to use a custom tika config
		if ( defined( 'WP_TIKA_CONFIG_PATH' ) ) {
			return constant( 'WP_TIKA_CONFIG_PATH' );
		}

		return WP_TIKA_PLUGIN_DIR . '/tika-config.xml';
	}

	/**
	 * Determine whether the extraction process should be skipped
	 *
	 * @return bool
	 * @access public
	 * @uses WP_TIKA_EXTRACT_DISABLE
	 */
	public function get_tika_extract_disable() {
		// set this to disable tika for expensive batch processes
		if ( defined( 'WP_TIKA_EXTRACT_DISABLE' ) ) {
			return boolval( constant( 'WP_TIKA_EXTRACT_DISABLE' ) );
		}

		return false;
	}

	/**
	 * Instance of a options object
	 *
	 * @var array
	 * @access protected
	 */
	protected $_options;

	/**
	 * Get the full settings object or a setting within the object
	 *
	 * @param string $key [ null ] - the key to grab or the full object
	 * @return string|array
	 */
	public function get_option( $key = null ) {
		if ( ! isset ( $this->_options ) ) {
			$this->_options = get_option( $this->option_key, $this->defaults );

			/**
			 * Filter the selected store object to enforce a storage via code
			 *
			 * @param string
			 * @return string
			 * @filter wptika/store
			 */
			$this->_options['store'] = apply_filters( 'wptika/store', $this->_options['store'] );

			/**
			 * Filter the enabled mime_types
			 *
			 * @param array
			 * @return array
			 * @filter wptika/mime_types
			 */
			$this->_options['mime_types'] = apply_filters( 'wptika/mime_types', $this->_options['mime_types'] );
		}

		return ! empty( $key ) && isset( $this->_options[ $key ] ) ? $this->_options[ $key ] : $this->_options;
	}

	/**
	 * Get a list of all supported mime types
	 *
	 * @return array
	 */
	public function get_mime_types() {
		$mime_types = array_filter( wp_get_mime_types(), function( $type ) {
			return ! preg_match( '/^audio|video/', $type );
		} );

		/**
		 * Add support for additional mime types through wptika/mime_types/available
		 *
		 * @param array $mime_types
		 * @return array
		 * @filter wptika/mime_types/available
		 */
		return apply_filters( 'wptika/mime_types/available', $mime_types );
	}

	/**
	 * Initialize integrations
	 *
	 * @return void
	 * @action init
	 * @priority 99
	 */
	public function init() {
		/**
		 * Filter the available stores.
		 *
		 * @param array
		 * @return array
		 * @filter wptika/stores
		 * @see $this->stores for format
		 */
		$this->stores = apply_filters( 'wptika/stores', $this->stores );

		/**
		 * Default WordPress Search
		 */
		$this->plugins[ __NAMESPACE__ . '\Plugins\WordPress' ] = new Plugins\WordPress;

		if ( class_exists( '\SolrPower' ) ) {
			/**
			 * Solr Power Plugin by Pantheon
			 *
			 * @see https://wordpress.org/plugins/solr-power/
			 */
			$this->plugins[ __NAMESPACE__ . '\Plugins\SolrPower' ] = new Plugins\SolrPower;
		}

		if ( class_exists( '\SearchWP' ) ) {
			/**
			 * SearchWP
			 *
			 * @see https://searchwp.com
			 */
			$this->plugins[ __NAMESPACE__ . '\Plugins\SearchWP' ] = new Plugins\SearchWP;
		}

		if ( function_exists( '\enable_media_replace_init' ) ) {
			/**
			 * Enable Media Replace
			 *
			 * @see https://wordpress.org/plugins/enable-media-replace/
			 */
			$this->plugins[ __NAMESPACE__ . '\Plugins\EnableMediaReplace' ] = new Plugins\EnableMediaReplace;
		}

		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			// WP_CLI
			\WP_CLI::add_command( Plugins\WPCLI::COMMAND, __NAMESPACE__ . '\Plugins\WPCLI' );
		}

		/**
		 * Add any additional actions here
		 *
		 * @param \WPTika\Plugin
		 * @action wptika/init
		 */
		do_action_ref_array( 'wptika/init', [ &$this ] );
	}

	/**
	 * Install our table on activate
	 *
	 * @return void
	 */
	public static function activation_hook() {
		Store\Table::create_table();
		wp_cache_flush();
	}

	/**
	 * Flush the object cache on deactivate
	 *
	 * @return void
	 */
	public static function deactivation_hook() {
		// flush object cache
		wp_cache_flush();
	}

	/**
	 * Delete options and custom tables on plugin uninstall
	 *
	 * @return void
	 */
	public static function uninstall_hook() {
		// delete options and custom table
		Store\Table::drop_table();
	}

}
