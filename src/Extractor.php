<?php
/**
 * This class manages the extaction process for the UI and for CLI
 *
 * @package  WPTika
 */

namespace WPTika;

class Extractor {

	/**
	 * How many items will be batched at a time
	 *
	 * @var int
	 * @access private
	 */
	private $batch_size;

	/**
	 * The array of documents in the queue
	 *
	 * @var array
	 * @access private
	 */
	private $queue = [];

	/**
	 * Set the batch size on construct
	 *
	 * @param integer $batch_size
	 */
	public function __construct( int $batch_size = 10 ) {
		$this->batch_size = $batch_size;
	}

	/**
	 * Add all posts to the queue for extraction
	 *
	 * @return array
	 * @access public
	 */
	public function queue_all() {
		$query = new \WP_Query( [
			'post_type'      => 'attachment',
			'post_status'    => [ 'publish', 'inherit' ],
			'fields'         => 'ids',
			'posts_per_page' => -1,
			'post_mime_type' => wp_tika_get_option( 'mime_types' ),
		] );

		$this->queue = $query->posts;

		return $query->posts;
	}

	/**
	 * Queue all unextracted attachments from the configured store
	 *
	 * @return void
	 */
	public function queue_missing() {
		$this->queue = wp_tika_get_queue();

		return $this->queue;
	}

	/**
	 * Dynamic getter for private properties
	 *
	 * @param string $prop
	 * @return mixed
	 */
	public function __get( $prop ) {
		if ( 'total' === $prop ) {
			return count( $this->queue );
		}

		if ( isset( $this->$prop ) ) {
			return $this->$prop;
		}
	}

	/**
	 * Process the queue in batches
	 *
	 * @return array of \WPTika\Attachment
	 * @access public
	 */
	public function batch() {
		$results = [];
		$batch_count = min( $this->batch_size, count( $this->queue ) );

		for( $i = 0; $i < $batch_count; $i++ ) {
			$id = array_shift( $this->queue );

			$attachment = new Attachment( $id );
			$attachment->extract();
			$attachment->save();

			array_push( $results, $attachment );
		}

		return $results;
	}

	/**
	 * Is the queue empty?
	 *
	 * @return boolean
	 */
	public function is_empty() {
		return empty( $this->queue );
	}

	/**
	 * Manually set the queue to an array of ids
	 *
	 * @param array $queue
	 * @return array
	 * @access public
	 */
	public function set_queue( array $queue ) {
		$this->queue = $queue;
		return $this->queue;
	}
}
