import React from 'React';
import ReactDOM from 'ReactDOM';

const Log = ( { id, error, src } ) => (
	<li>
		[{id}] { error ? (
			<span style={ { color: 'red' } }>{ error }</span>
		) : (
			<span>Attachment content extracted from <a href={src} target="_blank">{src}</a></span>
		) }
	</li>
);

class Extractor extends React.Component {

	constructor( props ) {
		super( props );

		this.state = {
			total    : parseInt( props.total, 10 ) || 0,
			extracted: parseInt( props.extracted, 10 ) || 0,
			queue    : parseInt( props.queue, 10 ) || 0,
			batch    : 10,
			page     : 0,
			pages    : 0,
			mode     : 'all',
			url      : props.url,
			nonce    : props.nonce,
			progress : 0,
			started  : false,
			finished : false,
			label    : '',
			log      : [],
		};
	}

	log( msg ) {
		const log = Array.from( this.state.log );
		log.unshift( msg );
		this.setState( { log } );
	}

	start() {
		let state = {
			page: 1,
		}

		switch( this.state.mode ) {
			case 'all':
				state.pages = Math.max( 1, Math.ceil( this.state.total / this.state.batch ) );
			break;
			case 'missing':
				state.pages = Math.max( 1, Math.ceil( this.state.queue / this.state.batch ) );
			break;
		}

		state.label = `Batch ${state.page} of ${state.pages}`;

		this.setState( state, this.extract.bind(this) );
	}

	finish() {
		const errors = this.state.log.filter( l => l.error );

		this.setState( {
			progress: 100,
			label: `Complete! Extacted ${ this.state.log.length - errors.length } attachments with ${ errors.length } ${ errors.length === 1 ? 'error' : 'errors' }`,
			finished: true,
		} );
	}

	next() {
		let progress;

		switch( this.state.mode ) {
			case 'all':
				progress = ( this.state.page / Math.ceil( this.state.total / this.state.batch ) ) * 100;
			break;
			case 'missing':
				progress = ( this.state.page / Math.ceil( this.state.queue / this.state.batch ) ) * 100;
			break;
		}

		this.setState( {
			label: `Batch ${this.state.page + 1} of ${this.state.pages}`,
			page: this.state.page + 1,
			progress
		}, this.extract.bind(this) );
	}

	extract() {
		this.setState( { started: true } );

		window.fetch( this.state.url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			body: `batch=${encodeURIComponent(this.state.batch)}&mode=${encodeURIComponent(this.state.mode)}&_wpnonce=${encodeURIComponent(this.state.nonce)}`
		} )
		.then( response => response.json() )
		.then( response => {
			if ( ! response.success ) {
				if ( response.data ) {
					return this.error( response.data );
				}

				return this.error( 'There was an unknown error extracting attachment content.' );
			}

			if ( Array.isArray( response.data.results ) ) {
				console.log(response.data.results);
				response.data.results.forEach( attachment => {
					this.log( {
						id: attachment.post_id,
						error: attachment.error,
						src: attachment.src,
					} );
				} );
			}

			if ( response.data.isEmpty ) {
				this.finish();
			} else {
				this.next();
			}
		} )
	}

	render() {
		const onSubmit = (e) => {
			e.preventDefault();
			this.setState( { mode: this.state.queue > 0 ? 'missing' : 'all' }, this.start.bind(this) );
		}

		console.log(this.state);

		return (
			<>
				<form onSubmit={ onSubmit }>
					<table className="form-table">
						<tr>
							<th>Total Attachments</th>
							<td>{ this.state.total }</td>
						</tr>
						<tr>
							<th>Extracted Attachments</th>
							<td>{ this.state.total - this.state.queue }</td>
						</tr>
						<tr>
							<th>Unextracted Attachments</th>
							<td>{ this.state.queue }</td>
						</tr>
						<tr>
							<th>Batch Size</th>
							<td>
								<input
									type="number"
									min="1"
									max="250"
									value={ this.state.batch }
									onChange={ (e) => this.setState( { batch: e.target.value } ) }
									disabled={ this.state.started }
								/>
							</td>
						</tr>
					</table>
				</form>
				<p>
					{ this.state.queue > 0 ? (
						<button
							className="button button-primary"
							disabled={ this.state.started }
							onClick={ () => this.setState( { mode: 'missing' }, this.start.bind(this) ) }
						>
							Extract Missing Attachments
						</button>
					) : null }

					<button
						className={ this.state.queue > 0 ? 'button' : 'button-primary' }
						disabled={ this.state.started }
						onClick={ () => this.setState( { mode: 'all' }, this.start.bind(this) ) }
					>
						Extract All Attachments
					</button>
				</p>

				{ this.state.started ? (
					<table className="form-table">
						<tr>
							<th>
								<span className="spinner" style={ { visibility: this.state.started && ! this.state.finished ? 'visible' : 'hidden' } }></span>
								<span>{ this.state.label }</span>
							</th>
							<td>
								<progress max="100" value={ this.state.progress } />
							</td>
						</tr>
						<tr>
							<th></th>
							<td>
								<ol id="tika-extractor-log" reversed>
									{ this.state.log.map( log => <Log {...log} /> ) }
								</ol>
							</td>
						</tr>
					</table>
				) : null }
			</>
		);
	}
}

const init = function() {
	const form = document.getElementById('tika-extractor');
	return ReactDOM.render( <Extractor {...form.dataset} />, form );
}

export default init();