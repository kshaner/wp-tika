# WP Tika

The goal of the project is to integrate the WordPress Media Library with Apache Tika for extracting document contents.

## Requirements

* Java
* A Tika Jar executable
* shell_exec enabled in php

## Installation

1. Install Java using either the [OpenJDK](https://openjdk.java.net/install/) toolkit,  or the official [Oracle](https://docs.oracle.com/javase/8/docs/technotes/guides/install/linux_jdk.html) binaries or your flavor of package manager. The `java` executable just needs to be accessible.

2. Install Tika by download the app jar from the [apache archive](https://tika.apache.org/download.html).  Place this somewhere within the web server, but not in the web root!  Make it executable by

```shell
$ wget https://archive.apache.org/dist/tika/tika-app-1.20.jar
$ chmod +x tika-app-1.20.jar
$ mv tika-app-1.20.jar /usr/local/bin
```

3. Configure WP Tika in wp-config.php

```php
// the path to the java executable - defaults to 'java' being in the PATH
define( 'WP_TIKA_JAVA_PATH', 'java' );

// the path to the tika jar file
define( 'WP_TIKA_PATH', '/usr/local/bin/tika-app-1.20.jar' );

// use a custom tika config xml file - default value shown
define( 'WP_TIKA_CONFIG_PATH', 'wp-content/plugins/wp-tika/tika-config.xml' );

// disable extracting content for large batch processes - remember to re-extract when the process is over using `wp tika extract`
define( 'WP_TIKA_EXTRACT_DISABLE', 'true' );
```

4. (Optional) OCR text recognition can be added by installing [tesseract](https://wiki.apache.org/tika/TikaOCR) and selecting image/* mime types. It is not installed by default because it is slow only relatively accurate.

Note: if installing from git, you will need to run `composer install` from inside the wp-tika directory.

## Usage

Text will be extracted when uploading files to the Media Library and saved to the WordPress database. A field for viewing/editing the extracted contents is available on the Media edit screen.

By default, the plugin extracts content from the following mime types:

* application/msword
* application/onenote
* application/oxps
* application/pdf
* application/rtf
* application/ttaf+xml
* application/vnd.apple.keynote
* application/vnd.apple.numbers
* application/vnd.apple.pages
* application/vnd.ms-access
* application/vnd.ms-excel
* application/vnd.ms-excel.addin.macroEnabled.12
* application/vnd.ms-excel.sheet.binary.macroEnabled.12
* application/vnd.ms-excel.sheet.macroEnabled.12
* application/vnd.ms-excel.template.macroEnabled.12
* application/vnd.ms-powerpoint
* application/vnd.ms-powerpoint.addin.macroEnabled.12
* application/vnd.ms-powerpoint.presentation.macroEnabled.12
* application/vnd.ms-powerpoint.slide.macroEnabled.12
* application/vnd.ms-powerpoint.slideshow.macroEnabled.12
* application/vnd.ms-powerpoint.template.macroEnabled.12
* application/vnd.ms-project
* application/vnd.ms-word.document.macroEnabled.12
* application/vnd.ms-word.template.macroEnabled.12
* application/vnd.ms-write
* application/vnd.ms-xpsdocument
* application/vnd.oasis.opendocument.chart
* application/vnd.oasis.opendocument.database
* application/vnd.oasis.opendocument.formula
* application/vnd.oasis.opendocument.graphics
* application/vnd.oasis.opendocument.presentation
* application/vnd.oasis.opendocument.spreadsheet
* application/vnd.oasis.opendocument.text
* application/vnd.openxmlformats-officedocument.presentationml.presentation
* application/vnd.openxmlformats-officedocument.presentationml.slide
* application/vnd.openxmlformats-officedocument.presentationml.slideshow
* application/vnd.openxmlformats-officedocument.presentationml.template
* application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
* application/vnd.openxmlformats-officedocument.spreadsheetml.template
* application/vnd.openxmlformats-officedocument.wordprocessingml.document
* application/vnd.openxmlformats-officedocument.wordprocessingml.template
* application/wordperfect
* text/calendar
* text/css
* text/csv
* text/html
* text/plain
* text/richtext
* text/tab-separated-values
* text/vtt

## Filters & Actions

### Filters

* `wptika/capability` - the capability needed to access the settings page [default: manage_options]
* `wptika/mime_types/available` - the full list of available mime types to index
* `wptika/mime_types` - the list of available mime types

### Actions

* `wptika/extract` - called after successfully extracting content
* `wptika/delete` - called after deleting an attachment with extracted text

## Integrations

### WordPress Search

When using the default WordPress search additional join/like statments are added to WP_Query search SQL statement.  This should just work.

### SolrPower

The plugin integrates with the [SolrPower plugin](https://wordpress.org/plugins/solr-power/) by adding attachments to Solr index and indexing the extracted contents for individual attachments.

If hosted on Pantheon, the proper path to Tika will be automatically picked up.  See https://pantheon.io/docs/external-libraries/#apache-tika

### Enable Media Replace

When the [Enable Media Replace](https://wordpress.org/plugins/enable-media-replace/) plugin is installed, attachment contents are re-extracted upon replacing.

### WP CLI

A WP CLI command for interacting with this plugin is included at `wp tika`.

#### Commands:

`wp tika extract [<id>...] [--all] [--progress] [--show]`

Extract contents from the media library to be indexed

`wp tika info [--format=<format>]`

Get information about the Tika configuration

`wp tika mime_types [--active] [--format=<format>]`

Show a list of available or active mime types

## Future Integrations

* SearchWP
* WPSolr
* Relevanssi
